#ifndef HEALTHREGENERATIONEFFECT_H
#define HEALTHREGENERATIONEFFECT_H



#include "effect.h"
#include "world/creature/creature.h"



class HealthRegenerationEffect : public Effect
{
public:
    HealthRegenerationEffect(Creature & creature);

    void affect() override;
};

#endif // HEALTHREGENERATIONEFFECT_H
