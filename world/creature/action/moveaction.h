#ifndef MOVEACTION_H
#define MOVEACTION_H



#include "action.h"
#include "world/creature/creature.h"



class MoveAction : public Action
{
public:
    MoveAction(Creature & creature);

    void do_action() override;

    void move();
    int move_x();
    int move_y();

    int is_moving();
};

#endif // MOVEACTION_H
