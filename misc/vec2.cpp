#include "vec2.h"



Vec2::Vec2()
    : x{0}, y{0}
{

}



Vec2::Vec2(int x, int y)
    : x{x}, y{y}
{
    this->set(x, y);
}



Vec2::Vec2(Vec2 & copy)
    : x{copy.get_x()}, y{copy.get_y()}
{

}



void Vec2::set(int x, int y)
{
    this->x = x;
    this->y = y;
}



void Vec2::set_x(int x)
{
    this->x = x;
}



void Vec2::set_y(int y)
{
    this->y = y;
}



int Vec2::get_x()
{
    return this->x.load();
}



int Vec2::get_y()
{
    return this->y.load();
}
