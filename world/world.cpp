#include "world.h"



World::World(int x, int y)
{
    this->entity_id = 0;

    auto now = std::chrono::system_clock::now().time_since_epoch();
    this->current_time = std::chrono::duration_cast<std::chrono::milliseconds>(now);

    for (int i=0; i<x; i++)
    {
        for (int j=0; j<y; j++)
        {
            this->tiles[i][j].reset(new Tile());
        }
    }
}



World::~World()
{
#ifdef debug
    std::cout << "world: delete" << std::endl;
#endif
}



void World::add_entity(std::shared_ptr<Entity> entity)
{
#ifdef debug
    std::cout << "world: add entity" << std::endl;
#endif

    this->entities.insert(std::make_pair(entity.get()->get_id(), entity));

    Vec2 pos = entity.get()->get_position();
    this->tiles[pos.get_x()][pos.get_y()].get()->add_entity(entity);
}



void World::delete_entity(unsigned int entity_id)
{
#ifdef debug
    std::cout << "world: delete entity" << std::endl;
#endif

    auto entity_ptr = this->entities.find(entity_id);

    if (entity_ptr != this->entities.end())
    {
        auto pos = entity_ptr->second->get_position();
        this->tiles[pos.get_x()][pos.get_y()].get()->remove_entity(entity_id);
        this->entities.erase(entity_id);
    }
}



void World::move_entity(std::shared_ptr<Entity> entity, int x, int y)
{
#ifdef debug
    std::cout << "world: move entity" << std::endl;
#endif

    auto tile_x = this->tiles.find(x);

    if (tile_x != this->tiles.end())
    {
        auto tile_y = tile_x->second.find(y);

        if (tile_y != tile_x->second.end())
        {
            if (this->tiles[x][y].get()->add_entity(entity))
            {
                auto pos = entity.get()->get_position();
                this->tiles[pos.get_x()][pos.get_y()].get()->remove_entity(entity.get()->get_id());
                entity.get()->get_position().set(x, y);
            }
        }
    }
}





void World::add_creature(std::shared_ptr<Creature> creature)
{
    this->creatures.insert(std::make_pair(creature.get()->get_id(), creature));
}



void World::delete_creature(unsigned int creature_id)
{
    auto creature_ptr = this->creatures.find(creature_id);
    if (creature_ptr != this->creatures.end())
    {
        this->creatures.erase(creature_id);
    }
}



void World::update()
{
#ifdef debug
    std::cout << "world: update world" << std::endl;
#endif

    auto now = std::chrono::system_clock::now().time_since_epoch();

    if (this->current_time + std::chrono::milliseconds(10) <= now)
    {
        this->current_time = std::chrono::duration_cast<std::chrono::milliseconds>(now);

        this->update_creatures();
    }
}



void World::update_creatures()
{
#ifdef debug
    std::cout << "world: update creatures" << std::endl;
#endif

    std::vector<std::future<void>> futures;

    for (auto creature: this->creatures)
    {
        futures.push_back(std::async([&](){std::static_pointer_cast<Creature>(creature.second)->update();}));
        //creature.second.get()->update();
    }

    for (uint i=0; i<futures.size(); i++)
    {
        futures[i].get();
    }
}



void World::update_entities()
{
    for (auto entity: this->entities)
    {
        entity.second.get()->update();
    }
}



unsigned int World::new_entity_id()
{
    this->entity_id++;
    return this->entity_id;
}



std::chrono::milliseconds & World::get_current_time()
{
    return this->current_time;
}



Entities & World::get_creatures()
{
    return this->creatures;
}



Entities & World::get_entities()
{
    return this->entities;
}



Tiles & World::get_tiles()
{
    return this->tiles;
}


