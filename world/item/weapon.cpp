#include "weapon.h"

Weapon::Weapon(World & world)
    : Item(world)
{
    this->type = "weapon";
}



int Weapon::get_ranged()
{
    return this->ranged;
}



int Weapon::get_attack()
{
    return this->attack;
}
