#include "action.h"



Action::Action(Creature & creature)
    : creature(creature)
{

}



void Action::do_action()
{

}



void Action::update()
{
    if (this->is_update_time())
    {
        this->do_action();

        this->next_update = this->creature.world.get_current_time() + ms(this->base_speed.count() - this->speed_reduction);
    }
}



bool Action::is_update_time()
{
    if (this->creature.world.get_current_time() >= this->next_update)
    {
        return true;
    }
    else
    {
        return false;
    }
}


