#include "entity.h"



Entity::Entity(const Entity & obj)
    : world(obj.world)
{
#ifdef debug
    std::cout << "entity: copy initialize" << std::endl;
#endif
}



Entity::Entity(World & world)
    :   world(world)
{
#ifdef debug
    std::cout << "entity: initialize" << std::endl;
#endif

    this->time_to_update = ms(0);
    this->entity_id = world.new_entity_id();
    this->position.set(0, 0);
}



Entity::~Entity()
{

}



void Entity::update()
{
#ifdef debug
    std::cout << "entity: update" << std::endl;
#endif
}



void Entity::set_update_time(ms time)
{
#ifdef debug
    //std::cout << "entity: set update time" << std::endl;
#endif

    if (this->time_to_update < this->world.get_current_time())
    {
        this->time_to_update = this->world.get_current_time();
    }
    else
    {
        if (this->time_to_update > time)
        {
            this->time_to_update = time;
        }
    }
}



unsigned int Entity::get_id()
{
    return this->entity_id;
}



Vec2 & Entity::get_position()
{
    return this->position;
}



void Entity::set_obstacle(bool b)
{
    this->obstacle = b;
}



bool Entity::is_obstacle()
{
    return this->obstacle;
}



void Entity::set_on_ground(bool b)
{
    this->on_ground = b;
}



bool Entity::is_on_ground()
{
    return this->on_ground.load();
}



std::string & Entity::get_type()
{
    return this->type;
}



World & Entity::get_world()
{
    return this->world;
}


