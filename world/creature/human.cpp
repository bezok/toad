#include "human.h"



Human::Human(World & world)
    : Creature(world)
{

}



int Human::hit()
{  
    if (!this->equipment["right_hand"])
    {
        if (this->equipment["right_hand"].get()->get_type() == "weapon")
        {
            int damage;

            auto weapon = std::static_pointer_cast<Weapon>(this->equipment["right_hand"]);

            if (weapon.get()->get_ranged())
            {
                damage = weapon.get()->get_attack() * this->abilities["distance"];
            }
            else
            {
                damage = weapon.get()->get_attack() * this->abilities["melee"] * this->attributes["strength"];
            }

            return damage;
        }
    }

    return 0; //Creature::hit();
}



int Human::take_hit(int damage)
{
    if (this->equipment["left_hand"])
    {
        if (this->equipment["left_hand"].get()->get_type() == "shield")
        {
            //damage -= this->shielding * this->left_hand.get()->get_defense();
        }
    }

    return Creature::take_hit(damage);
}



void Human::initialize_abilities()
{
    this->abilities.insert(std::make_pair("distance", 1));
    this->abilities.insert(std::make_pair("melee", 1));
    this->abilities.insert(std::make_pair("shielding", 1));
}



void Human::initialize_equipment()
{
    this->equipment.insert(std::make_pair("armor", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("boots", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("backpack", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("helmet", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("legs", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("left_hand", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("right_hand", std::shared_ptr<Item>()));

    this->equipment.insert(std::make_pair("first_weapon", std::shared_ptr<Item>()));
    this->equipment.insert(std::make_pair("second_weapon", std::shared_ptr<Item>()));
}



std::shared_ptr<Item> Human::wear_eq(std::string eq_part, std::shared_ptr<Item> item)
{
    if (this->equipment.find(eq_part) != this->equipment.end())
    {
        this->equipment[eq_part].swap(item);
    }

    return item;
}
