#include "misc/vec2.h"
#include "world.h"

#ifndef ENTITY_H
#define ENTITY_H



#include <memory>



//#define debug
#ifdef debug
    #include <iostream>
#endif



typedef std::chrono::milliseconds ms;



class World;



class Entity
{
public:
    Entity(const Entity & obj);
    Entity(World & world);

    ~Entity();

    virtual void update();

    void set_update_time(ms time);

    unsigned int get_id();
    Vec2 & get_position();

    void set_obstacle(bool b);
    bool is_obstacle();

    void set_on_ground(bool b);
    bool is_on_ground();

    std::string & get_type();

    World & get_world();

protected:
    unsigned int entity_id;
    bool obstacle;
    std::atomic<bool> on_ground;
    Vec2 position;
    std::string type;
    World & world;

    ms time_to_update;
};

#endif // ENTITY_H
