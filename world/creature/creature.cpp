#include "creature.h"



Creature::Creature(World & world)
    : Entity(world)
{
#ifdef debug
    std::cout << "creature: initialize" << std::endl;
#endif

    this->attack_to = 0;

    this->base_attack_speed = 1;
    this->base_move_speed = 1;
    this->base_regeneration = 1;
}



Creature::~Creature()
{
#ifdef debug
    std::cout << "creature: delete" << std::endl;
#endif

}



void Creature::initialize_attributes()
{
    this->attributes.insert(std::make_pair("agility", 0));
    this->attributes.insert(std::make_pair("durability", 0));
    this->attributes.insert(std::make_pair("strength", 0));
    this->attributes.insert(std::make_pair("vitality", 0));
    this->attributes.insert(std::make_pair("armor", 0));
    this->attributes.insert(std::make_pair("health", 0));
    this->attributes.insert(std::make_pair("speed", 0));
}



void Creature::initialize_times()
{
    this->times.insert(std::make_pair("actions_update", ms(0)));
    this->times.insert(std::make_pair("effects_update", ms(0)));
}



void Creature::update()
{
#ifdef debug
    std::cout << "creature: update" << std::endl;
#endif

    if (this->world.get_current_time() >= this->time_to_update)
    {
        //this->update_attack();
        //this->update_stats();
        this->update_effects();
        //this->update_move();
    }
}



void Creature::update_actions()
{
    if (this->world.get_current_time() >= this->times["actions_update"])
    {
        for (auto action: this->actions)
        {
            action.second->update();
        }
    }
}



void Creature::update_effects()
{
    std::vector<int> to_delete;

    if (this->world.get_current_time() >= this->times["effects_update"])
    {
        for (unsigned int i=0; i<this->effects.size(); i++)
        {
            this->effects[i].get()->update();
            to_delete.push_back(i);
        }

        for (auto it: to_delete)
        {
            this->delete_effect(it);
        }
    }
}



void Creature::set_attack(unsigned int entity_id)
{
    this->attack_to = entity_id;
}



void Creature::set_move(int x, int y)
{
    this->move_to.set(x, y);
}



void Creature::add_effect(std::shared_ptr<Effect> effect)
{
    this->effects.push_back(effect);
}



void Creature::delete_effect(int index)
{
    this->effects.erase(this->effects.begin() + index);
}



bool Creature::is_time(ms & time)
{
    return 0; // todo
}



int Creature::take_hit(int damage)
{
    damage = damage - this->attributes["armor"];
    this->attributes["health"] -= damage;
    return damage;
}



int Creature::get_attribute(std::string attribute)
{
    if (this->attributes.find(attribute) != this->attributes.end())
    {
        return this->attributes[attribute];
    }
    else
    {
        return -1;
    }
}


