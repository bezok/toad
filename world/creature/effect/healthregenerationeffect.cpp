#include "healthregenerationeffect.h"



HealthRegenerationEffect::HealthRegenerationEffect(Creature & creature)
    : Effect(creature)
{

}



void HealthRegenerationEffect::affect()
{
    if (this->creature.attributes["health"] < this->creature.attributes["durability"])
    {
        this->creature.attributes["health"]++;
    }
}
