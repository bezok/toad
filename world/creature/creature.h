#include "world/entity.h"



#ifndef CREATURE_H
#define CREATURE_H



#include <atomic>
#include <chrono>
#include <vector>



#include "misc/vec2.h"
#include "action/action.h"
#include "effect/effect.h"



//#define debug

#ifdef debug
#include <thread>
#include <iostream>
#endif



class Action;
class Effect;



typedef std::chrono::milliseconds ms;



class Creature : public Entity
{
public:
    Creature(World & world);

    ~Creature();

    void initialize_attributes();
    void initialize_times();

    void update();
    void update_actions();
    void update_effects();

    void set_attack(unsigned int entity_id);
    void set_move(int x, int y);

    void add_effect(std::shared_ptr<Effect> effect);
    void delete_effect(int index);

    bool is_time(ms & time);
    int take_hit(int damage);

    int get_attribute(std::string attribute);

    friend class Action;
    friend class AttackAction;
    friend class MoveAction;
    friend class UpdateStatsAction;
    friend class HealthRegenerationEffect;

protected:
    std::unordered_map<std::string, std::atomic<int>> attributes;
    std::unordered_map<std::string, std::shared_ptr<Effect>> base_effects;
    std::unordered_map<std::string ,std::shared_ptr<Action>> actions;
    std::vector<std::shared_ptr<Effect>> effects;
    std::unordered_map<std::string, ms> times;

    int base_attack_speed;
    int base_move_speed;
    int base_regeneration;

    std::atomic<unsigned int> attack_to;
    Vec2 move_to;
};

#endif // CREATURE_H
