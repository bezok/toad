#include "effect.h"



Effect::Effect(Creature & creature)
    : creature(creature)
{
    this->base_time = ms(1000);
}



void Effect::affect()
{

}



void Effect::update()
{
    auto time = this->creature.get_world().get_current_time();

    if (!this->expired)
    {
        if (this->infinite || (time > this->to_affect))
        {
            this->affect();

            if (time > this->end_time)
            {

                this->expired = true;
            }
            else
            {
                this->to_affect = time + this->base_time;
            }
        }
    }
}




int Effect::is_expired()
{
    return this->expired;
}
