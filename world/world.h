#include "misc/vec2.h"

#ifndef WORLD_H
#define WORLD_H



#include <chrono>
#include <memory>
#include <unordered_map>

#include <future>
#include <thread>



#include "creature/creature.h"
#include "entity.h"
#include "tile.h"



//#define debug



class Creature;
class Entity;
class Tile;



typedef std::unordered_map<unsigned int, std::shared_ptr<Entity>> Entities;
typedef std::chrono::milliseconds ms;
typedef std::unordered_map<int, std::unordered_map<int, std::shared_ptr<Tile>>> Tiles;



class World
{
public:
    World(int x, int y);
    ~World();

    void add_entity(std::shared_ptr<Entity> entity);
    void delete_entity(unsigned int entity_id);
    void move_entity(std::shared_ptr<Entity> entity, int x, int y);

    void add_creature(std::shared_ptr<Creature> creature);
    void delete_creature(unsigned int creature_id);

    void update();
    void update_creatures();
    void update_entities();

    unsigned int new_entity_id();

    ms & get_current_time();
    Entities & get_creatures();
    Entities & get_entities();
    Tiles & get_tiles();

protected:
    ms current_time;
    Entities creatures;
    Entities entities;
    unsigned int entity_id;
    Tiles tiles;

    std::atomic_flag creatures_lock;
};

#endif // WORLD_H
