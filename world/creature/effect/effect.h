#include "world/creature/creature.h"



#ifndef EFFECT_H
#define EFFECT_H



#include <atomic>
#include <chrono>
#include <memory>



class Creature;



typedef std::chrono::milliseconds ms;



class Effect
{
public:
    Effect(Creature & creature);

    virtual void affect();
    void update();

    int is_expired();

protected:
    std::string name;
    ms base_time;
    bool expired;
    int infinite;
    ms to_affect;
    ms end_time;
    Creature & creature;
};

#endif // EFFECT_H
