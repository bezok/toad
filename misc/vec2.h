#ifndef VEC2_H
#define VEC2_H


#include <atomic>


class Vec2
{
public:
    Vec2();
    Vec2(int x, int y);
    Vec2(Vec2 & copy);

    void set(int x, int y);
    void set_x(int x);
    void set_y(int y);

    int get_x();
    int get_y();

protected:
    std::atomic<int> x;
    std::atomic<int> y;
};

#endif // VECTOR_H
