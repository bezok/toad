#ifndef ITEM_H
#define ITEM_H



#include "world/entity.h"



class Item : public Entity
{
public:
    Item(World & world);

protected:
    int quality;
};

#endif // ITEM_H
