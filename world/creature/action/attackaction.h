#ifndef ATTACKACTION_H
#define ATTACKACTION_H



#include "action.h"
#include "world/creature/creature.h"



class AttackAction : public Action
{
public:
    AttackAction(Creature & creature);

    void do_action() override;

    void attack();
    int hit();

protected:

};

#endif // ATTACKACTION_H
