#ifndef UPDATESTATSACTION_H
#define UPDATESTATSACTION_H



#include "action.h"
#include "world/creature/creature.h"



class UpdateStatsAction : public Action
{
public:
    UpdateStatsAction(Creature & creature);

    void do_action() override;
};

#endif // UPDATESTATSACTION_H
