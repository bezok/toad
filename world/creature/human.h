#ifndef HUMAN_H
#define HUMAN_H



#include <memory>



#include "creature.h"
#include "world/item/item.h"
#include "world/item/weapon.h"



class Human : public Creature
{
public:
    Human(World & world);

    // overrided
    int hit();
    int take_hit(int damage);

    void initialize_abilities();
    void initialize_equipment();

    std::shared_ptr<Item> wear_eq(std::string eq_part, std::shared_ptr<Item> item);

protected:
    std::unordered_map<std::string, int> abilities;
    std::unordered_map<std::string, std::shared_ptr<Item>> equipment;
};

#endif // HUMAN_H
