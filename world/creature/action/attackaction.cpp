#include "attackaction.h"

AttackAction::AttackAction(Creature & creature)
    : Action(creature)
{
    this->creature.times.insert(std::make_pair("attack_update", ms(9)));
    this->speed_reduction = this->creature.attributes.find("speed")->second;
}



void AttackAction::do_action()
{
    this->attack();
}



void AttackAction::attack()
{
    auto entity_ptr = this->creature.world.get_creatures().find(this->creature.attack_to);

    if (entity_ptr != this->creature.world.get_creatures().end())
    {
        int damage = this->hit();

        std::static_pointer_cast<Creature>(entity_ptr->second)->take_hit(damage);
    }
}



int AttackAction::hit()
{
    return this->creature.attributes["strength"];
}
