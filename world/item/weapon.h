#ifndef WEAPON_H
#define WEAPON_H



#include "item.h"



class Weapon : public Item
{
public:
    Weapon(World & world);

    int get_ranged();
    int get_attack();

protected:
    int attack;
    int base_attack;
    int ranged;
    std::string ammunition;
};

#endif // WEAPON_H
