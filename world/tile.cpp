#include "tile.h"

Tile::Tile()
    : lock(false)
{
    this->lock.clear();
}



bool Tile::add_entity(std::shared_ptr<Entity> entity)
{
#ifdef debug
    std::cout << "tile: add entity" << std::endl;
#endif

    while(this->lock.test_and_set(std::memory_order_acquire))
    {
        std::this_thread::yield();
    }

    if (this->is_obstacle() && entity.get()->is_obstacle())
    {
        lock.clear(std::memory_order_release);

        return false;
    }
    else
    {
        this->entities.push_back(entity);
        lock.clear(std::memory_order_release);

        return true;
    }
}



void Tile::remove_entity(unsigned int entity_id)
{
    while(lock.test_and_set(std::memory_order_acquire))
    {
        std::this_thread::yield();
    }

    auto entity = std::find_if(this->entities.begin(), this->entities.end(), [entity_id](std::weak_ptr<Entity> e)->bool{return e.lock().get()->get_id() == entity_id;});

    if (entity != this->entities.end())
    {
        this->entities.erase(entity);
    }

    lock.clear(std::memory_order_release);
}



bool Tile::is_obstacle()
{
    for (auto entity: this->entities)
    {
        if (entity.lock().get()->is_obstacle())
        {
            return true;
        }
    }

    return false;
}
