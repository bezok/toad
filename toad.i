%module Toad
%{
#include "world/creature/action.h"
#include "world/entity.h"
#include "world/creature/creature.h"
#include "world/creature/human.h"
#include "world/item/item.h"
#include "world/item/weapon.h"
#include "world/world.h"
#include "misc/vec2.h"

%}



%include <std_shared_ptr.i>



%include "world.i"



namespace std
{
template<class Ty> class weak_ptr
    {
    public:
        typedef Ty element_type;

        weak_ptr();
        weak_ptr(const weak_ptr&);
        template<class Other>
        weak_ptr(const weak_ptr<Other>&);
        template<class Other>
        weak_ptr(const shared_ptr<Other>&);

        weak_ptr(const shared_ptr<Ty>&);


        void swap(weak_ptr&);
        void reset();

        long use_count() const;
        bool expired() const;
        shared_ptr<Ty> lock() const;
    };
}
