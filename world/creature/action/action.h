#include "world/creature/creature.h"

#ifndef ACTION_H
#define ACTION_H



#include <chrono>



typedef std::chrono::milliseconds ms;



class Creature;



class Action
{
public:
    Action(Creature & creature);

    virtual void do_action();
    void update();

    bool is_update_time();

protected:
    Creature & creature;

    int speed_reduction;
    ms base_speed;
    ms next_update;
};

#endif // ACTION_H
