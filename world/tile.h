#ifndef TILE_H
#define TILE_H



#include <algorithm>
#include <atomic>
#include <memory>
#include <vector>



#include "entity.h"



class Entity;



class Tile
{
public:
    Tile();

    bool add_entity(std::shared_ptr<Entity> entity);
    void remove_entity(unsigned int entity_id);

    bool is_obstacle();

protected:
    std::vector<std::weak_ptr<Entity>> entities;
    std::atomic_flag lock;
};

#endif // TILE_H
