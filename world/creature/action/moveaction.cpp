#include "moveaction.h"

MoveAction::MoveAction(Creature & creature)
    : Action(creature)
{
    this->creature.times.insert(std::make_pair("move_update", ms(0)));
    this->speed_reduction = this->creature.attributes.find("speed")->second;
}



void MoveAction::do_action()
{
    this->move();
}



void MoveAction::move()
{
    if (this->is_moving())
    {
        int x = this->move_x();
        int y = this->move_y();

        auto entity_ptr = this->creature.world.get_creatures().find(this->creature.get_id())->second;
        auto entity = std::static_pointer_cast<Entity>(entity_ptr);
        this->creature.world.move_entity(entity, this->creature.position.get_x() + x, this->creature.position.get_y() + y);
    }
}



int MoveAction::move_x()
{
    if (this->creature.move_to.get_x() - this->creature.position.get_x() > 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



int MoveAction::move_y()
{
    if (this->creature.move_to.get_y() - this->creature.position.get_y() > 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



int MoveAction::is_moving()
{
    if (this->creature.move_to.get_x() != this->creature.position.get_x())
    {
        return 1;
    }
    else if (this->creature.move_to.get_y() != this->creature.position.get_y())
    {
        return 2;
    }

    return 0;
}
